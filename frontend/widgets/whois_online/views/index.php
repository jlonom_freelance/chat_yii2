<?php
use yii;
use yii\helpers\Html;

?>
<h3><?= Yii::t("app", "Who is online"); ?></h3>
<?php
foreach($users as $user)
{
    echo Html::a($user->users->username, ["users/username/" . $user->users->username]);
}
?>