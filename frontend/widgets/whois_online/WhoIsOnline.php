<?php
namespace frontend\widgets\whois_online;
use frontend\models\Online;
use Yii;

class WhoIsOnline extends \yii\bootstrap\Widget {

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new Online;
        $users = $model->find()->where("datetime > :datetime", [":datetime" => date("Y-m-d H:i:s", time() - 60 * 60)])->with("users")->all();;
        return $this->render("index", ["users" => $users]);
    }
}