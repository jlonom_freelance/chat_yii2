<?php

namespace frontend\controllers;

use app\models\Messages;

class ChatController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Messages;
        $messages = $model->find()->with('users')->limit(50)->orderBy("id DESC")->all();
        return $this->render('index', array("messages" => $messages));
    }

}
