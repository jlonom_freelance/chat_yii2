<?php

namespace frontend\controllers;

use app\models\Messages;

class MessagesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetLastMessages()
    {
        $model = new Messages;
        if(isset($_POST['last_id']))
        {
            $results = $model::find("id>:messageID", array(":messageID" => $_POST['last_id']))
                ->with('user')
                ->limit(50);
            $last_id = 0;
            foreach($results as $res)
                $last_id = $res['id'];
            echo json_encode(
                array(
                    "html"  =>  $this->renderPartial(
                        'messages', array("messages" => $results)
                        ),
                    "last_id" => $last_id,
                )
            );
            die();
        }
        else
        {
            echo "die";
        }
    }

}
