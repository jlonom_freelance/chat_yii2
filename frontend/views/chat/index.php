<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\widgets\whois_online\WhoIsOnline;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
/* @var $form ActiveForm */
?>
<section class="chat-index">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-9">
            <div class="container-fluid">
                <div id="content">
                    <?php foreach($messages as $message): ?>
                        <?php $last_id = $message->id; ?>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4"><?=$message->users->username?></div>
                            <div class="col-xs-6 col-sm-4"><?=date("H:i:s", strtotime($message->time));?></div>
                            <div class="col-xs-12"><p><?=$message->message?></p></div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'form-new-message']); ?>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                    <?= Html::input("text", "message", null, ["class" => "form-control", "id" => "chat-message", "placeholder" => Yii::t('app', 'Enter Message')]) ?>
                    <div class="input-group-btn">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="hidden-xs col-sm-4 col-md-3">
            <?php echo WhoIsOnline::widget(); ?>
        </div>
    </div>

</section><!-- chat-index -->
<script>
    jQuery(document).ready(function($){
        var last_id = <?=$last_id?$last_id:0?>;
        $('#form-new-message').on('submit', function(event)
        {
            event.preventDefault();
            $.ajax({
                url:<?=Url::to(['message/add']);?>,
                type:"POST",
                dataType:"JSON",
                data:{
                    text:$("#chat-message").val(),
                    last_id:last_id
                },
                success:function(e)
                {
                    if(e.answer == "OK")
                    {
                        $("#content").append(e.html);
                    }
                    else
                        alert(e.error);
                }
            })
        });
    });
</script>