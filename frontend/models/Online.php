<?php
namespace frontend\models;

use common\models\User;
use Yii;

class Online extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%chat_online}}';
    }

    public function getUsers()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    public function rules()
    {
        return [
            [['user_id', 'datetime'], 'required'],
            [['user_id'], 'integer'],
            [['datetime'], 'safe'],
            [['user_id'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }
}