<?php

namespace app\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%chat_messages}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $is_private
 * @property integer $to_user
 * @property string $message
 * @property string $time
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_messages}}';
    }

    public function getUsers()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_private', 'to_user', 'message', 'time'], 'required'],
            [['user_id', 'is_private', 'to_user'], 'integer'],
            [['message'], 'string'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'is_private' => 'Is Private',
            'to_user' => 'To User',
            'message' => 'Message',
            'time' => 'Time',
        ];
    }
}
